public class ContaSalario extends Conta {
    private double limitedesaque;


    public ContaSalario(int numero, int agencia, String banco, double saldo, double sacar, double depositar, double limitedesaque) {
        super(numero, agencia, banco, saldo, sacar, depositar);
        this.limitedesaque = limitedesaque;
    }

    public double getLimitedesaque() {
        return limitedesaque;
    }

    public double getSaldo() {
        return this.saldo + this.limitedesaque ;
    }



    public double getSacar(){
        return this.sacar - this.saldo;
    }


    public double getDepositar() {
        return this.depositar + this.saldo;
    }
}