public class Testaconta {
    public static void main(String[] args) {
        ContaPoupanca p1 = new ContaPoupanca(22,3,"Banco CC",2800.00,7000.00,1.50,17/11/2001,3.00);
        System.out.println("O saldo da conta poupança é "+p1.getSaldo());
        System.out.println("Voce sacou "+p1.getSacar());
        System.out.println("Depositaram na sua conta "+p1.getDepositar());

        ContaCorrente c1 = new ContaCorrente(33,3,"Banco XX",300.00,100.00,200.00,500.00,3000.00);
        System.out.println("O saldo da sua Conta corrente é "+c1.getSaldo());
        System.out.println("O limite do saque é "+c1.getChequeEspecial());
        System.out.println("Voce sacou "+c1.getSacar());
        System.out.println("Depositaram na sua conta "+c1.getDepositar());

        ContaSalario s1 = new ContaSalario(44,6,"Banco AA",500.00,150.00,200.00,1500.00);
        System.out.println("O limite de Saque da conta Salario é "+s1.getLimitedesaque());
        System.out.println("Voce sacou "+s1.getSacar());
        System.out.println("Depositaram na sua conta "+s1.getDepositar());
    }
}