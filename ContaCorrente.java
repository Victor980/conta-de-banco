public class ContaCorrente extends Conta{
    private double chequeEspecial;
   private double limitedosaque;

    public ContaCorrente(int numero, int agencia, String banco, double saldo, double sacar, double depositar, double chequeEspecial, double limitedosaque) {
        super(numero, agencia, banco, saldo, sacar, depositar);
        this.chequeEspecial = chequeEspecial;
        this.limitedosaque = limitedosaque;
    }


    @Override
    public String toString() {
        return "ContaCorrente{" +
                "saldo=" + saldo +
                '}';
    }


    public double getChequeEspecial() {
        return this.chequeEspecial + this.saldo;
    }

    public double getSaldo(){
        return this.saldo;
    }



    public double getSacar(){
        return this.sacar - this.saldo;
    }


    public double getDepositar() {
        return this.depositar + this.saldo;
    }
}