public class ContaPoupanca extends Conta {
    private int diaAniversario;
    private double taxaDeJuros;


    public ContaPoupanca(int numero, int agencia, String banco, double saldo, double sacar, double depositar, int diaAniversario, double taxaDeJuros) {
        super(numero, agencia, banco, saldo, sacar, depositar);
        this.diaAniversario = diaAniversario;
        this.taxaDeJuros = taxaDeJuros;
    }

    public double getSaldo() {
        return this.saldo + this.taxaDeJuros * this.saldo;

    }

   public double getSacar(){
       return this.sacar - this.saldo;
   }


    public double getDepositar() {
        return this.depositar + this.saldo;
    }
}
